
import './App.css';
import Body from './BaiTapThucHanhLayout/Body';
import Footer from './BaiTapThucHanhLayout/Footer';
import Header from './BaiTapThucHanhLayout/Header';

function App() {
  return (
    <div className='App'>
      <Header></Header>
      <Body></Body>
      <Footer></Footer>
    </div>
  );
}

export default App;
